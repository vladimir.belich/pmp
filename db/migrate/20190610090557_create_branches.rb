class CreateBranches < ActiveRecord::Migration[5.2]
  def change
    create_table :branches do |t|
      t.string :name
      t.belongs_to :project, index: true, foreing_key: true
    end
  end
end
