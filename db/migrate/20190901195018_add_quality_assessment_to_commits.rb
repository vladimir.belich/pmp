class AddQualityAssessmentToCommits < ActiveRecord::Migration[5.2]
  def change
    add_column :commits, :quality_assessment, :decimal, precision: 5, scale: 2, null: false, default: 0
  end
end
