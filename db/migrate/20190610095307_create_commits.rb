class CreateCommits < ActiveRecord::Migration[5.2]

  def change
    create_table :authors do |t|
      t.string :name
      t.string :email
    end

    create_table :commits do |t|
      t.string :message
      t.string :sha
      t.belongs_to :branch, index: true, foreing_key: true
      t.belongs_to :author, index: true, foreing_key: true
      t.datetime :date
    end
  end
end
