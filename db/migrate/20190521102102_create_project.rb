class CreateProject < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name, limit: 255
      t.string :description, limit: 255
      t.string :ref, limit: 255
    end
  end
end
