require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations' }

  resources :projects
  get 'home/index'

  # authenticated do
    root to: 'projects#index'
  # end

  # root to: 'home#index'

  mount Sidekiq::Web => '/sidekiq'
end
