require 'rails_helper'

describe ProjectsController, type: :controller do
	let(:user) { create :user }

	before { sign_in user }

	describe 'GET #index' do
		let!(:projects) { create_list :project, 2 }

		it 'returns projects list' do
			get :index, as: :json

			expect_status(200)
			expect_json_sizes(2)
		end
	end

	describe 'GET #show' do
		let(:project) { create :project, name: 'Project1', description: 'description', ref: 'git:gitlab' }

		it 'returns project by id' do
			get :show, params: { id: project.id }, as: :json

			expect_status(200)
			expect_json(id: project.id, name: 'Project1', description: 'description', ref: 'git:gitlab')
		end
	end

	describe 'POST #create' do
		let(:attributes) { Hash[name: 'Project1', description: 'description', ref: 'git:gitlab.com'] }

    it 'returns new project' do
      expect { post :create, params: { project: attributes }, as: :json }.to change{ Project.count }.by(1)

			expect_status(201)
			expect_json(name: 'Project1', description: 'description', ref: 'git:gitlab.com')
    end
  end

  describe 'PATCH #update' do
    let!(:project) { create :project, name: 'Project name', description: 'description', ref: 'git:gitlab.com' }
		let(:attributes) { Hash[name: 'New project name', description: 'new description']}

		it 'returns updated project' do
      patch :update, params: { id: project.id, project: attributes }, as: :json

			expect_status(200)
			expect_json(id: project.id, name: 'New project name', description: 'new description')
    end
  end

  describe 'DELETE #destroy' do
    let!(:project) { create :project }

    it 'delete project' do
      expect { delete :destroy, params: { id: project.id }, as: :json } .to change(Project, :count).by(-1)

			expect_status(204)
    end
  end
end
