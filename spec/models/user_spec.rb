require 'rails_helper'

describe User, type: :model do
  describe '#full_name' do
    let(:user) { create :user, first_name: 'Sam', last_name: 'Jackson' }

    subject { user.full_name }

    it 'returns correct full name' do
      is_expected.to eq('Sam Jackson')
    end
  end
end

