# frozen_string_literal: true

FactoryBot.define do
  factory :project do
    name { 'First project' }
    description { 'Description project' }
    sequence(:ref) { |n| "git@gitlab.com:username/project_#{n}.git" }
  end
end
