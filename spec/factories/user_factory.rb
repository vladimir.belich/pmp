# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    first_name { 'Sam' }
    last_name { 'Stroomer' }
    sequence(:email) { |n| "user#{n}@test.org" }
    password { 'secret' }
    password_confirmation { 'secret' }
  end
end
