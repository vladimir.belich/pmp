# frozen_string_literal: true

class LogLoadingJob < ApplicationJob
  queue_as :default

  def perform(project)
    LogLoading.call(project: project)
  end
end
