# frozen_string_literal: true

class ChartData
  attr_reader :project, :datasets

  def self.call(args)
    new(args).call
  end

  def initialize(project:)
    @project = project
    @datasets = []
  end

  def call
    {
      labels: project.commits.pluck(Arel.sql("to_char(date, 'Mon YYYY')")).uniq,
      datasets: prepared_datasets
    }
  end

  def prepared_datasets
    project.branches.find_each do |branch|
      datasets << Hash[label: branch.name,
                       backgroundColor: 'rgb(255, 255, 255, 0.2)',
                       borderColor: "rgba(#{Random.rand(255)}, #{Random.rand(255)}, #{Random.rand(255)}, 1)",
                       data: data_ary(branch)]
    end

    datasets
  end

  def data_ary(branch)
    scores_hash = project.commits.pluck(Arel.sql("to_char(date, 'Mon YYYY'), 0")).uniq.to_h
    scores_hash.merge!(ary_quality(branch).to_h).values
  end

  def ary_quality(branch)
    branch.commits.pluck(Arel.sql("to_char(commits.date, 'Mon YYYY'), quality_assessment"))
  end
end
