# frozen_string_literal: true

class LogLoading
  attr_reader :project, :git
  require 'rubycritic/rake_task'
  require 'rake'

  def self.call(args)
    new(args).call
  end

  def initialize(project:)
    @project = project
    @git = Git.clone(project.ref, project.name, path: 'clones/')
  end

  def call
    branches.each do |branch_name|
      branch = create_branch(branch_name)
      git.checkout(branch_name)
      git.log(10_000).each { |log| create_commit(branch, log) }
    end

    delete_files
  end

  def branches
    @branches ||= git.branches.remote.map(&:name).delete_if { |name| name.include?('HEAD') }
  end

  def create_branch(name)
    project.branches.create!(name: name)
  end

  def create_author(author)
    Author.find_or_create_by!(name: author.name, email: author.email)
  end

  def commit_attributes(log)
    {
      message: log.message,
      sha: log.sha,
      author: create_author(log.author),
      date: log.date,
      quality_assessment: project_score
    }
  end

  def create_commit(branch, log)
    git.checkout(log.sha)
    branch.commits.find_or_create_by!(commit_attributes(log))
  end

  def project_score
    `rubycritic clones/#{project.name} --format json`

    file = File.read('tmp/rubycritic/report.json')
    json = JSON.parse(file)
    json['score']
  end

  def delete_files
    FileUtils.rm_r "clones/#{project.name}"
  end
end
