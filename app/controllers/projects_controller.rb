# frozen_string_literal: true

class ProjectsController < ApplicationController
  before_action :current_project, only: %i[show edit update destroy]
  before_action :authenticate_user!, only: %i[new create edit update destroy]
  respond_to :html, :json

  def index
    @projects = Project.all
    respond_with(@projects)
  end

  def show
    @data = ChartData.call(project: current_project)
    respond_with(current_project)
  end

  def new
    @current_project = Project.new
  end

  def create
    @current_project = Project.create(project_params)
    LogLoadingJob.perform_later(@current_project)

    respond_with(@current_project)
  end

  def edit; end

  def update
    current_project.update(project_params)

    respond_to do |format|
      format.html { respond_with(current_project) }
      format.json { respond_with(current_project, json: current_project) }
    end
  end

  def destroy
    current_project.destroy
    respond_with(current_project)
  end

  private

  def current_project
    @current_project ||= Project.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:name, :description, :ref)
  end
end
