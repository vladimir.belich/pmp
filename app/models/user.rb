# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  enum role: { regular: 'regular', manager: 'manager' }

  has_attached_file :avatar, styles: { medium: '300x300>', thumb: '32x32>' }, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :avatar, content_type: %r{\Aimage/.*\z}

  def full_name
    "#{first_name} #{last_name}"
  end
end
