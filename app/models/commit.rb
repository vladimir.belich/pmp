# frozen_string_literal: true

class Commit < ApplicationRecord
  has_many :branches
  belongs_to :author

  validates :sha, uniqueness: { scope: :branch_id }
end
