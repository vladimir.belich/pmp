# frozen_string_literal: true

class Branch < ApplicationRecord
  has_many :commits, -> { order(id: :desc) }, dependent: :destroy
  belongs_to :project
end
