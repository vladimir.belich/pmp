# frozen_string_literal: true

class Author < ApplicationRecord
  has_many :commits, dependent: :destroy

  validates :name, :email, presence: true
end
