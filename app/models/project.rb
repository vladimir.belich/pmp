# frozen_string_literal: true

class Project < ActiveRecord::Base
  has_many :branches, dependent: :destroy
  has_many :commits, -> { order(id: :desc) }, through: :branches

  validates :ref, uniqueness: true
  validates :name, :description, :ref, presence: true
  validates :name, :description, :ref, length: { maximum: 255 }
end
